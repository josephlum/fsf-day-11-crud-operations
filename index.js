var express = require("express");
var app = express();

// require handlebars
var handlebars = require("express-handlebars");

// Ask our app to use handlebars as the template engine
// Default layout to use is main
app.engine("handlebars", handlebars({
    defaultLayout: "main",
    helpers: {
        isActive: function(view,actual){
            if (view == actual)
                return("active");
            return("");
        }
    }
    })
);

// To run it...
app.set("view engine", "handlebars");

// Load my dbconfig
var config = require("./modules/dbconfig");

var mysql = require("mysql");
var connPool = mysql.createPool(config);

app.get("/", function(req,res){
    // console.info(">>> %s", req.originalUrl);
    res.render("index");
});

/*
app.get("/menu/devices", function(req,res){
    // console.info(">>> %s", req.originalUrl);
    res.render("devices");
});
*/

app.get("/search", function(req,res){
    connPool.getConnection(function(err,conn){
        if (err) {
            res.render("error", { error: err});
            return;
        }
        var q = "select * from devices where name like '%"
            + req.query.devicename + "%'";
        var paramQuery = "select * from devices where name like ? ";
        console.info("query: %s", q);
    try {
        conn.query(paramQuery, ["%" + req.query.devicename + "%"],
            function (err, rows) {
                if (err) {
                    console.info("query has error: %s", err);
                    res.render("/index");
                    return;
                }
                res.render("devices", {devices: rows});
            });
    }   catch(ex) {
        res.render("error", {error: ex});
    }   finally {
                conn.release();
    }
    });
});

// Load body-parser for handling application/x-www-form-urlencoded
var bodyParser = require("body-parser");
app.use(urlencodedParser = bodyParser.urlencoded({extended: false}));

app.post("/adddevice", function(req,res){
    connPool.getConnection(function(err,conn) {
        var insert = "insert into devices values (?,?,?,?,?,?,?)";
        conn.query(insert, [req.body.id, req.body.name, req.body.type, req.body.status,
            req.body.measure, req.body.date, req.body.remark], function (err, result) {
            if (err) {
                console.error("Error Inserting Record: %s", err);
            }
            console.info("Successfully added.");
            conn.release();
            res.redirect("/menu/devices");
        });
    });
});


// Combine the view with the main handlebar
// GET '/'
app.get("/menu/:category", function(req,res){
    console.info(">>> category %s", req.params.category);
    var cat = req.params.category;
    switch(cat) {
        case "index":
            res.render("index");
            break;
        case "devices":
            // Get a connection from the pool
            connPool.getConnection(function(err,conn){
                // If there are errors, report it
                if(err){
                    console.err("get connection error: %s", err);
                    res.render("/index");
                    return;
                }
                // Perform the query
                conn.query("select * from devices", function(err,rows){
                    if(err) {
                        console.query("query has error: %s", err);
                        res.render("/index");
                        return;
                    }
                    for (var i in rows)
                            console.info("deviceid: %s, name: %s", rows[i].deviceid, rows[i].name);
                    res.render("devices",{
                        devices: rows });
                    });
                    // Release the connection ! important
                    conn.release;
                });
            break;
        case "dashboard":
        case "config":
            res.render(cat);
            break;
        default:
            res.redirect("/");
    }
});

app.use(express.static(__dirname + "/public"));

// To redirect all unknown pages to index.html (error handler)
app.use(function(req,res,next){
    console.error("File not found: %s", req.originalUrl);
    res.redirect("/");
});

app.listen(3000, function(){
    console.info("Application started on port 3000");
});