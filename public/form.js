(function() {

    var JumboApp = angular.module("JumboApp", []);
    var JumboCtrl = function(){
        var vm = this;
        vm.name = "";
        vm.email = "";
        vm.mobile = "";
        vm.comment = "";
        vm.register = function(){
            vm.user = {
                name: vm.name,
                email:vm.email,
                mobile:vm.mobile,
                comment:vm.comment,
                submitted: false
            }
        };
        vm.submit = function(){
            vm.submitted = true;
        };
    };
    JumboApp.controller("JumboCtrl", [JumboCtrl]);
})();