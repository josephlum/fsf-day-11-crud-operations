var lightswitch1 = {
    deviceid: "switch1",
    name: "Relay Switch",
    devicetype: "Analog",
    status: "Disconnected",
    measurement: "State",
    date: "4-12-2016",
    remarks: "Light Switch"
};

var lightsensor1 = {
    deviceid: "light1",
    name: "Light Sensor",
    devicetype: "Analog",
    status: "Connected",
    measurement: "Brightness",
    date: "4-12-2016",
    remarks: "Light"
};

var device = function (id, name, type,stat,measure,date,remarks) {
    this.deviceid = id,
    this.name = name,
    this.devicetype = type,
    this.status = stat,
    this.measurement = measure,
    this.date = date,
    this.remarks = remarks
}
