(function(){
    var routingApp = angular.module('routingApp',['ngRoute'])
        .config(['$routeProvider',function($routeProvider){
            $routeProvider.when('/', {
                template: '<h2>Home</h2>'
            })
                .when('/devices',{
                    template: '<h2>Devices</h2>'
                })
                .when('/dashboard',{
                    template: '<h2>Dashboard</h2>'
                })
                .when('/datalogic',{
                    template: '<h2>Data Logic</h2>'
                })
                .otherwise({redirectTo: '/'});
            }]);
})();
